<?php

namespace App;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = ['id'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function countTotal($amount)
    {
        $total = $this->price * $amount;

        return $total;
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

}
