<?php

namespace App\Http\Controllers\Admin;

use App\Group;
use App\Http\Controllers\Controller;
use App\Http\Requests\Group\StoreGroupRequest;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function index()
    {
        $groups = Group::all();
        return view('admin.groups.index', compact('groups'));
    }

    public function create()
    {
        return view('admin.groups.create');
    }

    public function store(Group $group, StoreGroupRequest $request)
    {
        $group->create($request->all());

        return redirect('/admin/groups');
    }

    public function destroy(Group $group)
    {
        $group->delete();

        return redirect('/admin/groups');
    }

    public function edit(Group $group)
    {
        return view('admin.groups.edit', compact('group'));
    }

    public function update(Group $group, Request $request)
    {
        $group->update($request->all());

        return redirect('/admin/groups');
    }
}
