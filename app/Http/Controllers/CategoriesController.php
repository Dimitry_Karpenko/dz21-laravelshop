<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\Category\StoreCategoryRequest;
use App\Http\Requests\Category\UpdateCategoryRequest;
use App\Post;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::all();

        return view('categories.index')->with(compact('categories'));
    }

    public function show(Category $category)
    {
        return view('categories.show')->with(compact('category'));
    }

    public function create()
    {
        return view('categories.create');
    }

    public function store(StoreCategoryRequest $request)
    {
        Category::create($request->all());

        return redirect('/admin/categories');
    }

    public function destroy(Category $category)
    {
        $category->delete();

        return redirect('/admin/categories');
    }

    public function edit(Category $category)
    {
        return view('categories.edit')->with(compact('category'));

    }

    public function update(Category $category, UpdateCategoryRequest $request)
    {

        $category->update($request->all());

        return redirect('/admin/categories');
    }
}
