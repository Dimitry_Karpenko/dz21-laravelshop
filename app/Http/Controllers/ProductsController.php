<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Http\Requests\Product\StoreProductRequest;
use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('products.index')->with(compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        return view('products.create')->with(compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
//        $this->validate($request, [
//           'title' => 'required|min:3',
//           'slug' => 'required|min:3|unique:products,slug',
//           'price' => 'required',
//           'description' => 'required|min:3',
//        ]);

        Product::create($request->all());
        return redirect('/products/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
//        $product = Product::find($id);

        return view('products.show')->with(compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
//        $product = Product::find($id);

        return view('products.edit')->with(compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
//        $this->validate($request, [
//            'title' => 'required|min:3',
//            'slug' => 'required|min:3|unique:products,slug,'.$product->id,
//            'price' => 'required',
//            'description' => 'required|min:3',
//        ]);

//        $product = Product::find($id);
        $product->update($request->all());

        return redirect('/admin/products/');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
//        $product = Product::find($id);
        $product->delete();

        return redirect('/admin/products/');
    }
}
