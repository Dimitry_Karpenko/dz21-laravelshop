<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\SignUpRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function signUp()
    {
        $parametres =
            [
                'client_id' => $_ENV['GIT_CLIENT_ID'],
                'redirect_uri' => $_ENV['GIT_REDIRECT_URI'],
                'scope' => 'read:user,user:email'
            ];

        $authGitUri = 'https://github.com/login/oauth/authorize?' . http_build_query($parametres);

        return view('users.sign-up', compact('authGitUri'));

    }

    public function store(SignUpRequest $request)
    {
        $user = User::create($request->all(['name', 'email']) + ['password' => bcrypt($request->post('password'))]);

        $user->groups()->attach(2);

        Auth::login($user);


        return redirect()->route('products');
    }
}
