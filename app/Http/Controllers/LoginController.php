<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\SignInRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function destroy()
    {

        if (Auth::check()){
            Auth::logout();
        }

        return redirect()->route('products');

    }

    public function create()
    {
        $gitParametres =
            [
                'client_id' => $_ENV['GIT_CLIENT_ID'],
                'redirect_uri' => $_ENV['GIT_REDIRECT_URI'],
                'scope' => 'read:user,user:email'
            ];
        $googleParameters =
            [
                'redirect_uri' => $_ENV['GOOGLE_REDIRECT_URI'],
                'response_type' => 'code',
                'client_id' => $_ENV['GOOGLE_CLIENT_ID'],
                'scope' => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile',
            ];

        $authGoogleUri = 'https://accounts.google.com/o/oauth2/auth?' . http_build_query($googleParameters);

        $authGitUri = 'https://github.com/login/oauth/authorize?' . http_build_query($gitParametres);

        return view('users.sign-in', compact('authGitUri', 'authGoogleUri'));
    }

    public function store(SignInRequest $request)
    {

        if (Auth::attempt($request->all(['email', 'password']))){

            return redirect()->route('products');
        }



        return back();
    }

    public function gitAuth(Request $request)
    {
        $parameters =
            [
                'client_id' => $_ENV['GIT_CLIENT_ID'],
                'client_secret' => $_ENV['GIT_SECRET_ID'],
                'code' => $request->get('code'),
                'redirect_uri' => $_ENV['GIT_REDIRECT_URI'],
            ];

        $client = new \GuzzleHttp\Client();

        $response = $client->post('https://github.com/login/oauth/access_token',
        [
            'form_params' => $parameters,
        ]);

        parse_str($response->getBody()->getContents(), $result);

        $responseBase = $client->get('https://api.github.com/user',
            [
                'headers' => [
                    'Authorization' => 'token ' . $result['access_token'],
                ],
            ]
            );

        $responseBaseArr = json_decode($responseBase->getBody()->getContents(), true);

       $responseMail = $client->get('https://api.github.com/user/emails',
            [
                'headers' => [
                    'Authorization' => 'token ' . $result['access_token'],
                ],
            ]
            );

        $responseMailArr = json_decode($responseMail->getBody()->getContents(), true);

        $userName = $responseBaseArr['login'];
        $userMail = $responseMailArr[0]['email'];

        $user = User::where('email', $userMail)->first();

        if(isset($user->id))
        {
            Auth::login($user);

            return redirect()->route('products');
        }else{

            $user = User::create(['name' => $userName, 'email' => $userMail] + ['password' => bcrypt(rand(10,1000))]);

            $user->groups()->attach(2);

            Auth::login($user);

            return redirect()->route('products');
        }


    }

    public function googleAuth(Request $request)
    {
        $parameters =
            [
                'client_id' => $_ENV['GOOGLE_CLIENT_ID'],
                'client_secret' => $_ENV['GOOGLE_SECRET_ID'],
                'redirect_uri' => $_ENV['GOOGLE_REDIRECT_URI'],
                'grant_type'    => 'authorization_code',
                'code' => $request->get('code'),
            ];
        $client = new \GuzzleHttp\Client();

        $response = $client->post('https://accounts.google.com/o/oauth2/token',
            [
                'form_params' => $parameters,
            ]);


        $tokenInfo = json_decode($response->getBody()->getContents(), true);

        $parameters['access_token'] = $tokenInfo['access_token'];

        $userInfo = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo' . '?' . urldecode(http_build_query($parameters))), true);

        $userName = $userInfo['name'];
        $userMail = $userInfo['email'];

        $user = User::where('email', $userMail)->first();

        if(isset($user->id))
        {
            Auth::login($user);

            return redirect()->route('products');
        }else{

            $user = User::create(['name' => $userName, 'email' => $userMail] + ['password' => bcrypt(rand(10,1000))]);

            $user->groups()->attach(2);

            Auth::login($user);

            return redirect()->route('products');
        }





    }


}
