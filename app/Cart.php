<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{

    public static function getCartArray()
    {
        $cart = json_decode(request()->cookie('cart'), true);

        if (!is_array($cart)){
            $cart = [];
        }

        return $cart;
    }

    public static function addProduct(Product $product, $amount)
    {
        $cart = static::getCartArray();

        if (key_exists($product->id, $cart)){

            $cart[$product->id] += $amount;

        }else{
            $cart[$product->id] = $amount;
        }

        return $cart;

    }

    public static function getProductsFromCart()
    {
        $cartArr = static::getCartArray();
        $cartWithProducts = [];

        foreach($cartArr as $productId => $amount)
        {
            $cartWithProducts[] =
                [
                    'amount' => $amount,
                    'product' => Product::find($productId),
                    'total_price' => $amount * Product::find($productId)->price,
                ];
        }

        return $cartWithProducts;
    }

    public static function getTotalSum()
    {
        $totalSum = 0;

        $cartWithProducts = static::getProductsFromCart();

        foreach ($cartWithProducts as $product)
        {
            $totalSum += $product['total_price'];
        }

        return $totalSum;
    }


}
