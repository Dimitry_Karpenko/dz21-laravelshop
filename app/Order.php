<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = ['id'];

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('amount')->withTimestamps();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orderSum()
    {
        $sum = 0;

        foreach ($this->products as $product){
            $sum += $product->price * $product->pivot->amount;
        }

        return $sum;
    }
}
