<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['title', 'admin'];

    public function users()
    {

        return $this->belongsToMany(User::class);
    }
}
