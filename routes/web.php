<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',function (){
    return view('welcome');
});


Route::get('/products', 'ProductsController@index')->name('products');
Route::get('/products/{product}', 'ProductsController@show');





Route::get('/pages',  'PagesController@index');
Route::get('/pages/{page}', 'PagesController@show');

Route::get('/categories', 'CategoriesController@index');
Route::get('/categories/{category}', 'CategoriesController@show');


Route::get('/sign-up', 'UsersController@signUp');
Route::post('/sign-up', 'UsersController@store');

Route::get('/sign-in', 'LoginController@create')->name('login');
Route::post('/sign-in', 'LoginController@store');
Route::delete('/logout', 'LoginController@destroy');

Route::get('/gitauth', 'LoginController@gitAuth');
Route::get('/googleauth', 'LoginController@googleAuth');

Route::post('/cart/{product}', 'CartController@store');
Route::get('/cart', 'CartController@index');

Route::get('/order', 'OrdersController@create');
Route::post('/order', 'OrdersController@store');

Route::prefix('admin')->middleware('admin')->group(function (){

    Route::get('dashboard', 'Admin\AdminController@index');

    Route::get('/orders', 'Admin\OrderController@index');
    Route::get('/orders/{order}', 'Admin\OrderController@show');
    Route::delete('/orders/{order}', 'Admin\OrderController@destroy');


    Route::get('/products', 'Admin\ProductController@index');
    Route::get('/products/{product}/edit', 'ProductsController@edit');
    Route::post('/products', 'ProductsController@store');
    Route::delete('/products/{product}', 'ProductsController@destroy');
    Route::get('/products/create', 'ProductsController@create');
    Route::put('/products/{product}', 'ProductsController@update');

    Route::get('categories', 'Admin\CategoryController@index');
    Route::get('/categories/create', 'CategoriesController@create');
    Route::post('/categories', 'CategoriesController@store');
    Route::delete('/categories/{category}', 'CategoriesController@destroy');
    Route::get('/categories/{category}/edit', 'CategoriesController@edit');
    Route::put('/categories/{category}', 'CategoriesController@update');

    Route::get('pages', 'Admin\PageController@index');
    Route::get('/pages/create', 'PagesController@create');
    Route::post('/pages', 'PagesController@store');
    Route::delete('/pages/{page}', 'PagesController@destroy');
    Route::get('/pages/{page}/edit', 'PagesController@edit');
    Route::put('/pages/{page}', 'PagesController@update');

    Route::resource('groups', 'Admin\GroupController');
    Route::resource('users', 'Admin\UserController');

});


