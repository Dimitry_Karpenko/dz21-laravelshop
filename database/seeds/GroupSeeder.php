<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->insert([
            [
                'title' => 'admin',
                'admin' => true,
            ],
            [
                'title' => 'user',
                'admin' => false,
            ],
            [
                'title' => 'editor',
                'admin' => false,
            ],

        ]);
    }
}
