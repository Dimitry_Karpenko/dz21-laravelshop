@extends('layout.base');

@section('jumbotron')

    <section class="jumbotron text-center">
        <div class="container">

            <h1>Ваша корзина</h1>
            <p class="lead text-muted">Ваша корзина</p>

        </div>
    </section>

@endsection

@section('content')
    <table class="table">
        <tr>
            <th>Title</th>
            <th>Amount</th>
            <th>Price</th>
            <th>Total</th>
        </tr>


    @forelse($cart as $product)
        <tr>
            <td>{{$product['product']->title}}</td>
            <td>{{$product['amount']}}</td>
            <td>{{$product['product']->price}}</td>
            <td>{{$product['product']->countTotal($product['amount'])}}</td>
        </tr>
    @empty

        <tr>
            <td colspan="4"> Cart is empty</td>
        </tr>

    @endforelse
        <tr>
            <td colspan="3">Total sum:</td>
            <td>{{$totalSum}}</td>
        </tr>
    </table>


        <a class="btn btn-warning" href="/order" tabindex="-1" aria-disabled="true">Оформить заказ</a>


@endsection
