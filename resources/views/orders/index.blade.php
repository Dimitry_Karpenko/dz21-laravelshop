@extends('layout.base');

@section('jumbotron')

    <section class="jumbotron text-center">
        <div class="container">

            <h1>Ваша заказ</h1>
            <p class="lead text-muted">Ваша заказ</p>

        </div>
    </section>

@endsection

@section('content')
    <table class="table">
        <tr>
            <th>Title</th>
            <th>Amount</th>
            <th>Price</th>
            <th>Total</th>
        </tr>


        @forelse($cart as $product)
            <tr>
                <td>{{$product['product']->title}}</td>
                <td>{{$product['amount']}}</td>
                <td>{{$product['product']->price}}</td>
                <td>{{$product['product']->countTotal($product['amount'])}}</td>
            </tr>
        @empty

            <tr>
                <td colspan="4"> Cart is empty</td>
            </tr>

        @endforelse
        <tr>
            <td colspan="3">Total sum:</td>
            <td>{{$totalSum}}</td>
        </tr>
    </table>

    <form action="/order" method="post" >
        @csrf
        <div class="form-group">
            <label for="title">Комментарий</label>
            <textarea name="comment" id="" cols="30" class="form-control" rows="10"></textarea>
        </div>

        <div class="form-group">
            <button class="btn btn-success">Заказать</button>
        </div>

    </form>





@endsection
