@extends('layout.base')

@section('content')
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="links">
                        <a class="btn btn-dark" href="/products/">Products</a>
                        <a class="btn btn-dark" href="/order/">Orders</a>
                        <a class="btn btn-dark" href="/pages/">Pages</a>
                        <a class="btn btn-dark" href="/categories/">Categories</a>
                        <a class="btn btn-dark" href="/admin/dashboard/">Admin Cabinet</a>
                </div>
            </div>
        </div>
@endsection
