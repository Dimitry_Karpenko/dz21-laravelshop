@extends('layout.base')

@section('jumbotron')

    <section class="jumbotron text-center">
        <div class="container">
            <h1>Добавление нового товара</h1>
            <p class="lead text-muted">Добавление нового товара</p>
            <p>
                <a href="/products/" class="btn btn-primary my-2">Назад</a>
            </p>
        </div>
    </section>

@endsection

@section('content')
    <h1>{{$product->title}} - {{$product->price}} грн</h1>
    <p>Категория: <a href="/categories/{{$product->category->slug}}">{{$product->category->title}}</a> </p>
    <p>{{$product->description}}</p>
@endsection
