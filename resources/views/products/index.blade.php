@extends('layout.base');

@section('jumbotron')

    <section class="jumbotron text-center">
        <div class="container">
            <h1>Cписок товаров</h1>
            <p class="lead text-muted">Все товары интернет магазина</p>
            <p>
                @if(Auth::check() && Auth::user()->isAdmin())
                    <a href="/admin/products/create/" class="btn btn-primary my-2">Добавить новый товар</a>
                @endif
            </p>
        </div>
    </section>

@endsection

@section('content')

    @foreach($products as $product)
        <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg>
                <div class="card-body">
                    <h2>{{$product->title}}</h2>
                    <p class="card-text">Цена: {{$product->price}}</p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <a href="/products/{{$product->slug}}" class="btn btn-sm btn-outline-secondary">View</a>

                            @if(Auth::check() && Auth::user()->isAdmin())
                                <a href="/admin/products/{{$product->slug}}/edit" class="btn btn-sm btn-outline-secondary">Edit</a>
                                <form action="/admin/products/{{$product->slug}}" method="post">
                                    @csrf
                                    @method('delete')

                                    <input type="submit" class="btn btn-sm btn-outline-secondary" value="Delete">
                                </form>
                            @endif

                        </div>

                        <div class="btn-goup">
                            <form action="/cart/{{$product->slug}}" method="post">
                                @csrf
                                <input type="number" name="amount" class="btn btn-sm btn-outline-secondary" value="1">
                                <input type="submit" class="btn btn-success" value="Add to cart">
                            </form>
                        </div>
                        <small class="text-muted">9 mins</small>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
