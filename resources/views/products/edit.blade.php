@extends('admin.layout.base')

@section('jumbotron')

    <section class="jumbotron text-center">
        <div class="container">
            <h1>Редактирование товара</h1>
            <p class="lead text-muted">{{$product->title}}</p>
            <p>
                <a href="/products/" class="btn btn-primary my-2">Назад</a>
            </p>
        </div>
    </section>

@endsection

@section('content')

    <div class="col-md-6">

        @include('partials.errors')


        <form action="/admin/products/{{$product->slug}}" method="post">

            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control" value="{{$product->title}}">
            </div>

            <div class="form-group">
                <label for="slug">Slug</label>
                <input type="text" name="slug" id="slug" class="form-control" value="{{$product->slug}}">
            </div>

            <div class="form-group">
                <label for="price">Price</label>
                <input type="text" name="price" id="price" class="form-control" value="{{$product->price}}">
            </div>

            <div class="form-group">
                <label for="description">Description</label>
                <textarea type="text" name="description" id="description" class="form-control">{{$product->description}}</textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-success">Update</button>
            </div>

        </form>
    </div>
@endsection
