@extends('admin.layout.base')

@section('jumbotron')

    <section class="jumbotron text-center">
        <div class="container">
            <h1>Добавление нового товара</h1>
            <p class="lead text-muted">Добавление нового товара</p>
            <p>
                <a href="/products/" class="btn btn-primary my-2">Назад</a>
            </p>
        </div>
    </section>

@endsection

@section('content')

    <div class="col-md-6">

        @include('partials.errors')


        <form action="/admin/products" method="post">

            @csrf

            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control" value="{{old('title')}}">
            </div>

            <div class="form-group">
                <label for="slug">Slug</label>
                <input type="text" name="slug" id="slug" class="form-control" value="{{old('slug')}}">
            </div>

            <div class="form-group">
                <label for="price">Price</label>
                <input type="text" name="price" id="price" class="form-control" value="{{old('price')}}">
            </div>

            <div class="form-group">
                <select name="category_id" id="" class="form-control">

                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->title}}</option>
                    @endforeach

                </select>

                <label for="price"></label>
                <input type="text" name="price" id="price" class="form-control" value="{{old('price')}}">
            </div>

            <div class="form-group">
                <label for="description">Description</label>
                <textarea type="text" name="description" id="description" class="form-control">{{old('description')}}</textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-success">Create</button>
            </div>

        </form>
    </div>
@endsection
