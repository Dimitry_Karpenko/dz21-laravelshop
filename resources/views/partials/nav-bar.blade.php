<ul class="nav nav-pills">
    <li class="nav-item">
        <a class="nav-link active" href="/">Home</a>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Меню</a>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="/pages">Pages</a>
            <a class="dropdown-item" href="/products">Products</a>
            <a class="dropdown-item" href="/orders">Orders</a>
            <a class="dropdown-item" href="/categories">Categories</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Separated link</a>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
    </li>


        @guest
            <li class="nav-item">
                <a class="btn btn-primary" href="/sign-up" tabindex="-1" aria-disabled="true">Sign-Up</a>
            </li>

             <li class="nav-item">
                <a class="btn btn-success" href="/sign-in" tabindex="-1" aria-disabled="true">Sign-In</a>
             </li>

        @endguest

            @auth

            <li class="nav-item">
                <a class="nav-link active" href="" tabindex="-1" aria-disabled="true">Привет, {{Auth::user()->name}}</a>
            </li>

            <li class="nav-item">
                <form action="/logout" method="POST">

                    @method('delete')
                    @csrf

                    <button class="btn btn-danger">Log Out</button>

                </form>
            </li>

        <li class="nav-item">
            <a class="btn btn-info" href="/admin/dashboard/" tabindex="-1" aria-disabled="true">Admin Panel</a>
        </li>

        @endauth

    <li class="nav-item">
        <a class="btn btn-warning" href="/cart" tabindex="-1" aria-disabled="true">Cart</a>
    </li>

</ul>
