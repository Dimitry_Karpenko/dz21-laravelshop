<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.0.1">
    <title>Blog Template · Bootstrap</title>

    @include('admin.partials.header-connection')

</head>
<body>
<div class="container">

    @include('admin.partials.header')

    @include('admin.partials.navigation')

    @yield('jumbotron')

    @yield('content')


@include('admin.partials.footer');

</body>
</html>

