<div class="nav-scroller py-1 mb-2">
    <nav class="nav d-flex justify-content-between">
        <a class="p-2 text-muted" href="/admin/orders/">Orders</a>
        <a class="p-2 text-muted" href="/admin/products/">Products</a>
        <a class="p-2 text-muted" href="/admin/categories/">Categories</a>
        <a class="p-2 text-muted" href="/admin/pages/">Pages</a>
        <a class="p-2 text-muted" href="/admin/users/">Users</a>
        <a class="p-2 text-muted" href="/admin/groups/">Users Group</a>
    </nav>
</div>
