@extends('admin.layout.base')

@section('jumbotron')
    <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
            <h1 class="display-4 font-italic">Order # {{$order->id}} <br> from {{$order->user->name}}</h1>
            <form action="/admin/orders/{{$order->id}}" method="post">
                @csrf
                @method('delete')

                <input class="btn btn-danger" type="submit" value="delete">
            </form>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <table class="table table-striped">
            <tr>
                <th>Title</th>
                <th>Amount</th>
                <th>Price</th>
                <th>Total</th>
            </tr>

            @foreach($order->products as $product)
                <tr>
                    <th>{{$product->title}}</th>
                    <th>{{$product->pivot->amount}}</th>
                    <th>{{$product->price}} Uah</th>
                    <th>{{$product->price*$product->pivot->amount}} Uah</th>
                </tr>
            @endforeach
            <tr>
                <th>SUM</th>
                <th colspan="3" style="text-align: center">
                    {{$order->orderSum()}} Uah
                </th>
            </tr>




        </table>
    </div>

@endsection


