@extends('admin.layout.base')

@section('jumbotron')
    <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
            <h1 class="display-4 font-italic">You have {{$orders->count()}} orders </h1>


        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <table class="table table-striped">
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Comment</th>
                <th>Show</th>
                <th>Delete</th>
            </tr>

            @foreach($orders as $order)


                    <tr>
                        <td>{{$order->id}}</td>
                        <td>{{$order->user->name}}</td>
                        <td>{{$order->comment}}</td>
                        <td><a class="btn btn-info" href="/admin/orders/{{$order->id}}">Show</a></td>
                        <td>
                            <form action="/admin/orders/{{$order->id}}" method="post">
                                @csrf
                                @method('delete')

                                <input class="btn btn-danger" type="submit" value="delete">
                            </form>
                        </td>
                    </tr>


            @endforeach

        </table>
    </div>

@endsection

