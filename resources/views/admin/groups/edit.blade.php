@extends('admin.layout.base')

@section('jumbotron')
    <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
            <h1 class="display-4 font-italic">Edit Group</h1>


        </div>
    </div>
@endsection

@section('content')

    <div class="col-md-6">

        @include('partials.errors')


        <form action="/admin/groups/{{$group->id}}" method="post">

            @csrf
            @method('put')

            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control" value="{{$group->title}}">
            </div>

            @if(Auth::check() && Auth::user()->isAdmin())

                <div class="form-group">
                    <label for="admin">Admin</label>
                    <select name="admin" id="admin">
                        @if($group->admin)
                            <option value="1" selected>Yes</option>
                            <option value="0">No</option>
                        @else
                            <option value="1" >Yes</option>
                            <option value="0" selected>No</option>
                        @endif
                    </select>
                    @endif

                    <div class="form-group">
                        <button class="btn btn-success">Edit</button>
                    </div>

        </form>
    </div>
@endsection

