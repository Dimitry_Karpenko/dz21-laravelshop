@extends('admin.layout.base')

@section('jumbotron')
    <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
            <h1 class="display-4 font-italic">User groups</h1>

            <p ><a class="btn btn-info" href="/admin/groups/create" class="text-white font-weight-bold">Create group</a></p>
        </div>
    </div>
@endsection

@section('content')



    <div class="row mb-2">

        @foreach($groups as $group)

            <div class="col-md-6">
                <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
                    <div class="col p-4 d-flex flex-column position-static">
                        <strong class="d-inline-block mb-2 text-primary">Група пользователей</strong>
                        <h3 class="mb-0">{{$group->title}}</h3>
                        <div class="mb-1 text-muted">
                            Admin:
                            @if($group->admin)
                                Yes
                            @else
                                No
                            @endif

                        </div>

                        <a href="/admin/groups/{{$group->id}}/edit/" class="btn btn-dark">Edit</a>

                        <form action="/admin/groups/{{$group->id}}" method="post">

                            @csrf
                            @method('delete')

                            <input type="submit" value="Delete" class="btn btn-danger" style="width: 100%">

                        </form>
                    </div>
                    <div class="col-auto d-none d-lg-block">
                        <svg class="bd-placeholder-img" width="200" height="250" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg>
                    </div>
                </div>
            </div>

        @endforeach
    </div>



@endsection
