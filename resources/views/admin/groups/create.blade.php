@extends('admin.layout.base')

@section('jumbotron')
    <div class="jumbotron p-4 p-md-5 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
            <h1 class="display-4 font-italic">Create Group</h1>


        </div>
    </div>
@endsection

@section('content')

    <div class="col-md-6">

        @include('partials.errors')


        <form action="/admin/groups" method="post">

            @csrf

            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control" value="{{old('title')}}">
            </div>

            @if(Auth::check() && Auth::user()->isAdmin())

                <div class="form-group">
                    <label for="admin">Admin</label>
                    <select name="admin" id="admin">
                        <option value="1">Yes</option>
                        <option value="0" selected>No</option>
                    </select>
            @endif

                <div class="form-group">
                    <button class="btn btn-success">Create</button>
                </div>

        </form>
    </div>
@endsection

