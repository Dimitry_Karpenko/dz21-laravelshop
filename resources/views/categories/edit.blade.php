@extends('admin.layout.base')

@section('jumbotron')

    <section class="jumbotron text-center">
        <div class="container">
            <h1>Редактирование категори</h1>
            <p class="lead text-muted">{{$category->title}}</p>
            <p>
                <a href="/products/" class="btn btn-primary my-2">Назад</a>
            </p>
        </div>
    </section>

@endsection

@section('content')

    <div class="col-md-6">

        @include('partials.errors')


        <form action="/admin/categories/{{$category->slug}}" method="post">

            @csrf
            @method('PUT')

            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control" value="{{$category->title}}">
            </div>

            <div class="form-group">
                <label for="slug">Slug</label>
                <input type="text" name="slug" id="slug" class="form-control" value="{{$category->slug}}">
            </div>

            <div class="form-group">
                <label for="description">Description</label>
                <textarea type="text" name="description" id="description" class="form-control">{{$category->description}}</textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-success">Update</button>
            </div>

        </form>
    </div>
@endsection
