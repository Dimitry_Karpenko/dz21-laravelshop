@extends('layout.base');

@section('jumbotron')

    <section class="jumbotron text-center">
        <div class="container">
            <h1>Cписок категорий</h1>
            <p class="lead text-muted">Все категории товаров интернет магазина</p>
            <p>
                @if(Auth::check() && Auth::user()->isAdmin())
                    <a  href="/admin/categories/create/" class="btn btn-primary my-2">Добавить новую категорию</a>
                @endif

            </p>
        </div>
    </section>

@endsection

@section('content')

    @foreach($categories as $category)
        <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg>
                <div class="card-body">
                    <h2>{{$category->title}}</h2>

                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <a href="/categories/{{$category->slug}}" class="btn btn-sm btn-outline-secondary">View</a>

                            @if(Auth::check() && Auth::user()->isAdmin())

                                <a href="/admin/categories/{{$category->slug}}/edit" class="btn btn-sm btn-outline-secondary">Edit</a>
                                <form action="/admin/categories/{{$category->slug}}" method="post">
                                    @csrf
                                    @method('delete')

                                    <input type="submit" class="btn btn-sm btn-outline-secondary" value="Delete">
                                </form>

                            @endif

                        </div>
                        <small class="text-muted">9 mins</small>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
