@extends('admin.layout.base')

@section('jumbotron')

    <section class="jumbotron text-center">
        <div class="container">
            <h1>Добавление новой страницв</h1>
            <p class="lead text-muted">Добавление новой страницы</p>
            <p>
                <a href="/pages/" class="btn btn-primary my-2">Назад</a>
            </p>
        </div>
    </section>

@endsection

@section('content')

    <div class="col-md-6">

        @include('partials.errors')


        <form action="/admin/pages" method="post">

            @csrf

            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control" value="{{old('title')}}">
            </div>

            <div class="form-group">
                <label for="slug">Slug</label>
                <input type="text" name="slug" id="slug" class="form-control" value="{{old('slug')}}">
            </div>

            <div class="form-group">
                <label for="price">intro</label>
                <input type="text" name="intro" id="intro" class="form-control" value="{{old('intro')}}">
            </div>

            <div class="form-group">
                <label for="description">content</label>
                <textarea type="text" name="content" id="content" class="form-control">{{old('content')}}</textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-success">Create</button>
            </div>

        </form>
    </div>
@endsection
