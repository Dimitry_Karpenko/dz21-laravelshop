@extends('layout.base')

@section('jumbotron')
    <section class="jumbotron text-center">
        <div class="container">
            <h1>Cписок страниц</h1>
            <p class="lead text-muted">Все страницы интернет магазина</p>
            <p>

                @if(Auth::check() && Auth::user()->isAdmin())
                    <a href="/admin/pages/create/" class="btn btn-primary my-2">Добавить новую страницу</a>
                @endif

            </p>
        </div>
    </section>
@endsection

@section('content')
    @foreach($pages as $page)
        <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text></svg>
                <div class="card-body">
                    <h2>{{$page->title}}</h2>
                    <p class="card-text">Цена: {{$page->intro}}</p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <a href="/pages/{{$page->slug}}" class="btn btn-sm btn-outline-secondary">View</a>

                            @if(Auth::check() && Auth::user()->isAdmin())
                                <a href="/admin/pages/{{$page->slug}}/edit" class="btn btn-sm btn-outline-secondary">Edit</a>
                                <form action="/admin/pages/{{$page->slug}}" method="post">
                                    @csrf
                                    @method('delete')

                                    <input type="submit" class="btn btn-sm btn-outline-secondary" value="Delete">
                                </form>
                            @endif


                        </div>
                        <small class="text-muted">9 mins</small>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
