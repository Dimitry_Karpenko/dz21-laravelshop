@extends('layout.base')

@section('jumbotron')

    <section class="jumbotron text-center">
        <div class="container">
            <h1>{{$page->title}}</h1>
            <p class="lead text-muted">{{$page->intro}}</p>
            <p>
                <a href="/products/" class="btn btn-primary my-2">Назад</a>
            </p>
        </div>
    </section>

@endsection

@section('content')
   <p>{{$page->content}}</p>
@endsection

