@extends('admin.layout.base')

@section('jumbotron')

    <section class="jumbotron text-center">
        <div class="container">
            <h1>Редактировать страницу страницы</h1>
            <p class="lead text-muted">Редактирование страницы</p>
            <p>
                <a href="/pages/" class="btn btn-primary my-2">Назад</a>
            </p>
        </div>
    </section>

@endsection

@section('content')

    <div class="col-md-6">

        @include('partials.errors')


        <form action="/admin/pages/{{$page->slug}}" method="post">

            @csrf
            @method('put')

            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control" value="{{$page->title}}">
            </div>

            <div class="form-group">
                <label for="slug">Slug</label>
                <input type="text" name="slug" id="slug" class="form-control" value="{{$page->slug}}">
            </div>

            <div class="form-group">
                <label for="price">intro</label>
                <input type="text" name="intro" id="intro" class="form-control" value="{{$page->intro}}">
            </div>

            <div class="form-group">
                <label for="description">content</label>
                <textarea type="text" name="content" id="content" class="form-control">{{$page->content}}</textarea>
            </div>

            <div class="form-group">
                <button class="btn btn-success">Edit</button>
            </div>

        </form>
    </div>
@endsection
