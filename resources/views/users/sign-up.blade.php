@extends('layout.base');

@section('jumbotron')
    <section class="jumbotron text-center">
        <div class="container">
            <h1>Добавление нового пользователя</h1>
            <p class="lead text-muted">Добавление нового пользователя</p>
            <p>
                <a href="/products/" class="btn btn-primary my-2">Назад</a>
            </p>
        </div>
    </section>
@endsection

@section('content')
    <div>
        @include('partials.errors')
    </div>


    <form action="/sign-up" method="post">

        @csrf

        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
                <input name="name" type="text" class="form-control" id="name" value="{{old('name')}}">
            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input name="email" type="email" class="form-control" id="email" value="{{old('name')}}">
            </div>
        </div>


        <div class="form-group row">
            <label for="password" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
                <input name="password" type="password" class="form-control" id="password">
            </div>
        </div>

        <div class="form-group row">
            <label for="password_confirmation" class="col-sm-2 col-form-label">Confirm</label>
            <div class="col-sm-10">
                <input name="password_confirmation" type="password" class="form-control" id="password_confirmation">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Sign Up</button>
                <a href="{{$authGitUri}}" class="btn btn-primary">GitHub</a>
            </div>
        </div>
    </form>
@endsection
