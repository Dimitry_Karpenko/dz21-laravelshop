@extends('layout.base')

@section('jumbotron')
    <section class="jumbotron text-center">
        <div class="container">
            <h1>Вход в кабинет</h1>
            <p class="lead text-muted">Вход в кабинет</p>
            <p>
                <a href="/products/" class="btn btn-primary my-2">Назад</a>
            </p>
        </div>
    </section>
@endsection

@section('content')
    <div>
        @include('partials.errors')
    </div>


    <form action="/sign-in" method="post">

        @csrf

        <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input name="email" type="email" class="form-control" id="email" value="{{old('name')}}">
            </div>
        </div>


        <div class="form-group row">
            <label for="password" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
                <input name="password" type="password" class="form-control" id="password">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Sign In</button>
                <a href="{{$authGitUri}}" class="btn btn-primary">GitHub</a>
                <a href="{{$authGoogleUri}}" class="btn btn-primary">Google</a>
            </div>
        </div>
    </form>

@endsection
